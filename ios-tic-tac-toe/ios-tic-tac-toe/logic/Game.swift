//
//  Game.swift
//  ios-tic-tac-toe
//
//  Created by Matija Bartolac on 4/3/18.
//  Copyright © 2018 mbartolac. All rights reserved.
//

import Foundation

class Game {
    
    private static let STANDARD_BOARD: Int = 3
    
    private var board: Board
    private var currentPlayer: Player
    
    init() {
        board = Board(Game.STANDARD_BOARD)
        currentPlayer = arc4random() % 2 == 0 ? Player.PLAYER_1 : Player.PLAYER_2
    }
    
    private func checkWinner(_ lastMoveX: Int, _ lastMoveY: Int) -> Bool {
        var col: Int = 0
        var row: Int = 0
        var diag: Int = 0
        var opDiag: Int = 0
        let boardDimension = board.getBoardDimension()
        
        for i in 0...(boardDimension - 1) {
            if (board.playableArea[lastMoveX][i] == currentPlayer) { row += 1 }
            if (board.playableArea[i][lastMoveY] == currentPlayer) { col += 1 }
            if (board.playableArea[i][i] == currentPlayer) { diag += 1 }
            if (board.playableArea[i][boardDimension - i - 1] == currentPlayer) { opDiag += 1}
        }
        
        return (col == boardDimension || row == boardDimension || diag == boardDimension || opDiag == boardDimension)
    }
    
    public func nextMove(col: Int, row: Int) -> GameState {
        do {
            try board.processPlayerMove(player: currentPlayer, row: row, col: col)
        } catch {
            print("System has encountered unrecoverable error. \(error.localizedDescription)")
            exit(-1)
        }
        
        if (checkWinner(row, col)) { return currentPlayer == Player.PLAYER_1 ? GameState.WIN_P1 : GameState.WIN_P2 }
        if (board.isFull()) { return GameState.DRAW }
        
        changeCurrentPlayer()
        return GameState.PLAY
    }
    
    public func getCurrentPlayer() -> Player {
        return currentPlayer
    }
    
    private func changeCurrentPlayer() {
        self.currentPlayer = currentPlayer == Player.PLAYER_1 ? Player.PLAYER_2 : Player.PLAYER_1
    }
}
