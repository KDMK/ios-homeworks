//
//  GameState.swift
//  ios-tic-tac-toe
//
//  Created by Matija Bartolac on 4/3/18.
//  Copyright © 2018 mbartolac. All rights reserved.
//

import Foundation

enum GameState {
    case PLAY
    case WIN_P1
    case WIN_P2
    case DRAW
}
