//
//  GameController.swift
//  ios-tic-tac-toe
//
//  Created by Five on 4/3/18.
//  Copyright © 2018 mbartolac. All rights reserved.
//

import UIKit

class GameController: UIViewController {
    
    private let IMG_OKS = UIImage(named: "oks")
    private let IMG_IKS = UIImage(named: "iks")
    
    @IBOutlet weak var gameInfo: UILabel!
    @IBOutlet weak var btnRestart: UIButton!
    @IBOutlet var boardButtons: [UIButton]!
    
    private var game: Game
    
    @IBAction func restartButtonTap(_ button: UIButton) {
        button.setTitle("Restart game", for: .normal)
        self.game = Game() // we could nil all elements in current Game -> this is faster, but more memory consuming(if garbage collector doesn't do its job correctly)
        gameInfo.text = "It's player \(game.getCurrentPlayer().rawValue) turn"
        boardButtons.forEach { btn in
            btn.isUserInteractionEnabled = true
            btn.setImage(nil, for: .normal)
        }
    }
    
    @IBAction func boardButtonTap(_ button: UIButton) {
        let row = button.tag / 10 - 1
        let col = button.tag  % 10 - 1
        
        button.setImage(game.getCurrentPlayer() == .PLAYER_1 ? IMG_IKS : IMG_OKS, for: .normal)
        let result = game.nextMove(col: col, row: row)
        
        switch result {
        case .WIN_P1, .WIN_P2:
            updateViewAtGameEnd("Player \(game.getCurrentPlayer().rawValue) has won!")
        case .DRAW:
            updateViewAtGameEnd("It's a draw")
        default:
            gameInfo.text = "It's player \(game.getCurrentPlayer().rawValue) turn"
        }
    }
    
    private func updateViewAtGameEnd(_ info: String) {
        boardButtons.forEach { btn in btn.isUserInteractionEnabled = false }
        gameInfo.text = info
        btnRestart.setTitle("New game", for: .normal)
    }
    
    convenience init() {
        self.init(game: Game())
    }
    
    init(game: Game) {
        self.game = game
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        gameInfo.text = "It's player \(game.getCurrentPlayer().rawValue) turn"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
