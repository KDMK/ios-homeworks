//
//  Board.swift
//  ios-tic-tac-toe
//
//  Created by Matija Bartolac on 4/3/18.
//  Copyright © 2018 mbartolac. All rights reserved.
//

import Foundation

enum IndexOutOfBoundsError: Error {
    case runtimeError(String, Int, Int)
}

/**
    Board models simple board used in Tic-Tac-Toe game implementation.
    current model supports only square boards.
 **/
class Board {
    private let dimension: Int
    private var numOfEmptyFields: Int
    private(set) var playableArea: Array<Array<Player?>>
    
    init(_ dimension: Int) {
        self.dimension = dimension

        self.playableArea = Array<Array<Player?>>(repeating: Array<Player?>(repeating: nil, count: dimension), count: dimension)
        self.numOfEmptyFields = dimension * dimension
    }
    
    func getBoardDimension() -> Int {
        return self.dimension
    }
    
    func isFull() -> Bool {
        return numOfEmptyFields == 0
    }
    
    /**
     Method processes next player move putting it at the right postion in the
     board. If position is outside the board exception is thrown
     **/
    func processPlayerMove(player: Player, row: Int, col: Int) throws {
        if(!isPositionInsideBoard(row, col)) {
            throw IndexOutOfBoundsError.runtimeError("Index is outside of board bounds", row, col);
        }
        
        playableArea[row][col] = player
        numOfEmptyFields -= 1
    }
    
    /**
     Method checks if given board matrix index is valid. Returns true if
     index is inside the board.
    **/
    private func isPositionInsideBoard(_ row: Int,_ col: Int) -> Bool {
        return (row >= 0 && row < dimension) && (col >= 0 && col < dimension)
    }
}
