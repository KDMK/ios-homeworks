//
//  Player.swift
//  ios-tic-tac-toe
//
//  Created by Five on 4/3/18.
//  Copyright © 2018 mbartolac. All rights reserved.
//

import Foundation

enum Player: String {
    case PLAYER_1 = "X"
    case PLAYER_2 = "O"
}
