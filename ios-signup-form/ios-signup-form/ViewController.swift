//
//  ViewController.swift
//  ios-signup-form
//
//  Created by Matija Bartolac on 4/2/18.
//  Copyright © 2018 RUAZOSI. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var inputFirstName: UITextField!
    @IBOutlet weak var inputLastName: UITextField!
    @IBOutlet weak var inputEmail: UITextField!
    
    @IBAction func buttonClear(_ sender: UIButton) {
        print("\(String(describing: sender.currentTitle!)) button tap!")
        
        inputFirstName.text = ""
        inputLastName.text = ""
        inputEmail.text = ""
    }
    
    @IBAction func buttonSubmit(_ sender: UIButton) {
        print("\(String(describing: sender.currentTitle!)) button tap!")
        
        printTextValue(inputFirstName, label: "First name")
        printTextValue(inputLastName, label: "Last name")
        printTextValue(inputEmail, label: "Email")
    }
    
    private func printTextValue(_ textField: UITextField, label: String) {
        if let field = textField.text {
            print("\(label): \(field)")
        }
    }
}
